package com.indoornavigation.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.indoor.navigation.indoornavigation.R;
import com.indoornavigation.Model.BaseStation;

import java.util.ArrayList;

/**
 * Adapter to handle data from a checked list view.
 */
public class BsAdapterCheckbox extends ArrayAdapter<BaseStation> {

    private ArrayList<BaseStation> selectedBaseStations = new ArrayList<>();
    private ArrayList<BaseStation> data = new ArrayList<>();

    public BsAdapterCheckbox(Context context, ArrayList<BaseStation> baseStations) {
        super(context, 0, baseStations);
        this.data = baseStations;
        this.selectedBaseStations = new ArrayList<>(data);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.dialog_radiomap_item,
                    parent, false);
        }

        if (position < this.getCount()) {
            // Get the data item for this position
            final BaseStation baseStation = getItem(position);

            TextView tv = (TextView) convertView.findViewById(R.id.txtRadiomapDialog);
            CheckBox cBox = (CheckBox) convertView.findViewById(R.id.checkBox);

            if (tv != null) {
                tv.setText(baseStation.getSsid());
            }

            if (cBox != null) {
                if (this.selectedBaseStations.contains(baseStation))
                    cBox.setChecked(true);
                else
                    cBox.setChecked(false);
            }
        }
        return convertView;
    }

    public void toggleSelection(BaseStation bs) {
        if (this.selectedBaseStations.contains(bs))
            this.selectedBaseStations.remove(bs);
        else
            this.selectedBaseStations.add(bs);

        notifyDataSetChanged();
    }

    public ArrayList<BaseStation> getCheckedItems() {
        return this.selectedBaseStations;
    }
}
